# ThorFi V7 Diagrams

Gitlab Issue:

The following Diagrams were created using Mermaid.

**Mermaid Docs:** https://mermaid-js.github.io/mermaid/#/flowchart?id=interaction

## THOR.USD

```mermaid
flowchart LR;
    USDT--> id1(Median Price);
    USDC--> id1;
    BUSD --> id1;
    UST --> id1;
    id1 --> id2(Anchor Price);
```

**THOR.USD Mint/Burn**

```mermaid
    flowchart LR;
    id5(User) <--> id1(THOR.RUNE);
    id1(THOR.RUNE) <--> id2(Burn/Mint) <--> id3(THOR.USD) <--> id5(User);
    id2(Burn/Mint) -. fee .-> id4[/THOR.USD Vault\];

```

**Virtual THOR.USD / RUNE Pool**

```mermaid
flowchart LR;
id1(Anchor Price) -. Informs THOR.USD Depth .-> id2[(THOR.USD/RUNE Pool)];
id4[(Stable Coin Pools)] -. Combined Depths informs RUNE Depth .-> id2[(THOR.USD / RUNE Pool)];
id5((Swap User)) -. swap .-> id2[(THOR.USD / RUNE Pool)];
id2[(THOR.USD / RUNE Pool)] -. swap fees .-> id6[/THOR.USD Vault\];
```

**Price Manipulation Protection**

```mermaid
flowchart TD;
    Attacker --> USDT;
    Attacker --> USDC;
    Attacker --> BUSD;
    Defender -.-> BUSD;
```

**THOR.USD - Combined**

```mermaid
graph LR

    subgraph THOR.USD
        idUser((Minting User))
        idSwapUser((Swap User))
        idUser -- THOR.RUNE, THOR.USD ---> idBurnMint
        idAnchorPrice(Anchor Price) --> idBurnMint(Burn/Mint)
        idAnchorPrice -. Informs THOR.USD Depth .-> idThorUsdPool[(THOR.USD/RUNE Pool)]
        idSwapUser<-- swap ---> idThorUsdPool

            subgraph Mint/Burn
                idBurnMint -- fee --> idUsdSavingsVault[/THOR.USD Savings Vault\]
            end
            subgraph Virtual Pool

                id16[(Stable Coin Pools)] -. Combined Depths Inform RUNE Depth .-> idThorUsdPool
                idThorUsdPool -- swap fee --> idUsdSavingsVault

            end

    end


```

# Borrow

```mermaid
graph RL;
    subgraph Borrow
    id0((Borrow User)) -- deposits LP position as collateral --> idLend(Lending Module, holds LP Collateral)
    idLend --> idMint(Mint THOR.USD)
    idMint(Mint THOR.USD)-- Receives THOR.USD--> id0
    idMint -- fee --> idUsdVault[/THOR.USD Vault\]
    end
```

# THOR Savings (Interest Accounts)

**Savings - THOR.USD**

```mermaid
    graph RL
    subgraph Savings - THOR.USD
        idsaveUser1((Savings User)) -- THOR.USD deposit --> UsdVault[/THOR.USD Vault\]
        idMint(THOR.USD Burn/Mint) -- fees --> UsdVault
        idPool(THOR.USD / RUNE Pool) -- fees --> UsdVault
        UsdVault -. withdraw after 14 days .->idsaveUser1
        idEmissions(Network Emissions) -. boot strap .-> UsdVault
    end
```

**Savings - Blue Chip**

```mermaid
    graph RL
    subgraph Savings - Blue Chip
        idsaveUser2((Savings User))-- asset swapped for RUNE -->idMint(THOR.Asset, Minted)
        idMint -- deposit --> assetVault[/Asset Vault\]
        assetVault -. withdraw after 14 days .->idsaveUser2
        idBorrowYield(Loan LP Collateral)-- Yield --> assetVault
        idAnchor(Asset Anchor price) -. informs .-> idMint
        idMint -- fee --> assetVault
    end
```
