import random
import string

import numpy as np

from .model_data_classes import LpPosition

################################################################################
# Helpers
################################################################################


def _generate_borrower_list(n: int = 100):

    """
    Generates a list of lp positions to pass to sys_params.
    Each of these lp positions will be used as collateral for a loan,
    if the CR is considered desirable.
    """

    borrower_list = []

    for i in range(n + 1):

        lp_position = LpPosition(
            pool="BTC.BTC",
            lp_asset_amount=0.175,
            lp_rune_amount=1000,
            lp_units=77171162621,
        )

        borrower_list.append(lp_position)

    return borrower_list


def _generate_random_user_id():
    rand_4 = random.choices(string.ascii_uppercase, k=4)
    user_id = "user-" + "".join(rand_4)

    return user_id


def _calc_liquidity_units(P, R, A, r, a):
    """
    Calculate liquidity provider units
    slipAdjustment = (1 - ABS((R a - r A)/((r + R) (a + A))))
    units = ((P (a R + A r))/(2 A R))*slidAdjustment
    P = pool units
    R = pool rune balance after
    A = pool asset balance after
    r = provided rune
    a = provided asset
    """
    P = float(P)
    R = float(R)
    A = float(A)
    r = float(r)
    a = float(a)
    if R == 0.0 or A == 0.0 or P == 0:
        return int(r)

    slipAdjustment = 1 - abs((R * a - r * A) / ((r + R) * (a + A)))

    units = (P * (a * R + A * r)) / (2 * A * R)

    return int(units * slipAdjustment)


def calc_collat_ratio(P, C, uC, maxCR):
    """
    P = total rune in pool (pooled rune)
    C = total rune in pool as loan collateral (collateral rune)
    uC = user rune collateral
    maxCR = maximum collateralization ratio
    """
    CR = ((C + uC) / P * (maxCR - 1.0)) + 1.0

    return CR


def lend(r, a, R, A, Rv, Uv, pool_amp_factor):

    """
    r: rune deposit value
    a: asset deposit value
    R: rune depth in pool
    A: asset depth in pool
    Rv: virtual rune depth
    Uv: virtual  USD depth
    pool_amp_factor: pool amp factor will scale the debt minted.
            max value of 1, lower value will increase fee to thor.usd vault

    returns:
        d: total deposit value in rune
        u: debt minted for user, THOR.USD - this is the amount OWED to the protocol to get collat back.
    """

    d = r + (R / A * a)
    u = (d * Rv * Uv) / (d + Rv) ** 2 * pool_amp_factor

    return u  # this is the value owed to the network


################################################################################
# Lend Policy
################################################################################


def lend_policy(_params, substep, state_history, previous_state):
    """
    Takes users that indicate they want to borrow.
    Calculates all the values to pass to state_update fucntions as policy_input

    lend policy is used within a partial update block (calculations that happen within a block)
    """

    # Check if the previous block CR desireable.
    # If Greater, users are not willing to take loans.
    if previous_state["lend_module"].latest_CR > _params["max_desirable_CR"]:

        # since no loans only passing previous CR to next block.
        return {
            "update_cr": previous_state["lend_module"].latest_CR,
        }

    # TODO - how can I have a switch key to pay back or not...

    current_block = previous_state["timestep"]  # + 1 # do we need + 1 here?

    # import pdb; pdb.set_trace()

    borrower = _params["borrower"][current_block]
    # user = previous_state['users']['user1']
    btc_pool = previous_state["btc_pool"]
    thor_usd_pool = previous_state["thor_usd_pool"]
    # lend_module = previous_state["lend_module"]

    # Specify the LP position components
    rune_collat = borrower.lp_rune_amount  # half of LP position value
    btc_collat = borrower.lp_asset_amount  # other half of LP position value

    # collat_usd_value = (rune_collat * thor_usd_pool.asset_price_usd) + (
    #     btc_collat * btc_pool.asset_price_usd
    # )

    # get CR
    lend_cr = calc_collat_ratio(
        P=btc_pool.pool_rune_depth,
        C=btc_pool.pool_rune_collateral,
        uC=rune_collat,
        maxCR=_params["max_CR"],
    )

    # Get portion of the collateral protocol is willing to lend
    rune_lend = rune_collat / lend_cr
    btc_lend = btc_collat / lend_cr

    debt_owed = (rune_lend * thor_usd_pool.asset_price_usd) + (
        btc_lend * btc_pool.asset_price_usd
    )

    debt_minted = lend(
        r=rune_lend,  # in rune terms
        a=btc_lend,  # in asset terms
        R=btc_pool.pool_rune_depth,
        A=btc_pool.pool_asset_depth,
        Rv=thor_usd_pool.pool_rune_depth,  # RUNE in THOR.USD/RUNE pool
        Uv=thor_usd_pool.pool_asset_depth,  # THOR.USD in THOR.USD/RUNE pool
        pool_amp_factor=_params["pool_amp_factor"],
    )

    # Construct loan entry
    _loan_entry = {
        "user": _generate_random_user_id(),
        "block": current_block,
        "debt_minted": debt_minted,
        "debt_owed": debt_owed,
        "collat_rune": rune_collat,
        "collat_asset": btc_collat,
        "collat_lp_units": borrower.lp_units,
        "loan_cr": lend_cr,
    }

    # increase users debt owed, recived
    # increase the lend module blances
    # increase portion of the pool that is collateral
    # increase rune supply

    policy_update_values = {
        "update_user_debt_owed": debt_owed,  # in thor.usd terms
        "update_user_debt_received": debt_minted,  # in thor.usd terms
        "update_cr": lend_cr,  # user's collat ratio
        "update_increase_pool_debt_minted": debt_minted,
        "update_increase_pool_debt_owed": debt_owed,
        "update_increase_pool_rune_collateral": rune_collat,
        "update_increase_pool_asset_collateral": btc_collat,
        "update_rune_lend_inflation": debt_owed
        / thor_usd_pool.asset_price_usd,  # In Rune Terms
        "update_thor_usd_fee_revenue": np.round(debt_owed - debt_minted, 5),
        "update_loan_entry": _loan_entry,
    }

    return policy_update_values
