from dataclasses import dataclass, field
from typing import Dict, List


# TODO - add reasonable default values
@dataclass
class Pool:
    pool_asset_depth: float
    pool_rune_depth: float
    pool_asset_collateral: float
    pool_rune_collateral: float
    pool_units: int
    pool_yield: float  # percent
    asset_price_usd: float
    historic_slip: List = field(default_factory=list)


@dataclass
class RuneSupply:
    rune_supply: float  # total rune supply
    rune_lend_inflation: float
    rune_save_deflation: float


@dataclass
class LendModule:
    asset: str
    debt_minted: float  # what a user gets
    debt_owed: float  # how much the user owes
    latest_CR: float  # CR for current loans
    loans: Dict[str, Dict] = field(
        default_factory=dict
    )  # dict of dicts where key is user-block and value is loan info.


@dataclass
class Deposit:
    amount: float
    block: int


@dataclass
class ThorUsdVault:
    APY: float = 0.0
    fee_revenue: float = 0.0
    deposits: float = 0.0
    withdrawals: float = 0.0
    members: List[Deposit] = field(default_factory=list)


@dataclass
class LpPosition:
    pool: str
    lp_asset_amount: float  # portion of Lp in asset term
    lp_rune_amount: float  # portion of lp in rune term
    lp_units: float  # LP units representing asset/rune pair


@dataclass
class SavingsVault:
    asset: str
    APY: float
    deposits: float = 0.0
    withdrawals: float = 0.0
    members: Dict = field(default_factory=dict)  # user key, initial deposit value


# TODO - remove
@dataclass
class Thorchad:
    pool: str
    lp_asset_amount: float  # portion of Lp in asset term
    lp_rune_amount: float  # portion of lp in rune term
    debt_owed: float  # in thor.usd terms
    debt_received: float
    CR: float
    savings_asset: float
    savings_deposit: float
    asset_wallet: float
    asset_wallet_balance: float
    savings_deposit_block: float
    lp_units: float
