from utils.constants import BLOCK_TIME_SECONDS, SAVINGS_LOCK_UP_PERIOD
from utils.math_utils import calc_yield_from_apy

from .lend_policy import _generate_random_user_id
from .model_data_classes import LpPosition, Pool, ThorUsdVault

################################################################################
# Helpers
################################################################################


def _generate_saver_list(n: int = 100):

    """
    Generates a list of lp positions to pass to sys_params.
    Each of these lp positions will be used as a proxy for a savings user deposit,
    if the Savings Vault APY is considered desirable.
    """

    savers_list = []

    for i in range(n + 1):

        # Just an asset value
        saver = LpPosition(
            pool="BTC.BTC",
            lp_asset_amount=0.175,
            lp_rune_amount=0,
            lp_units=0,
        )

        savers_list.append(saver)

    return savers_list


def calc_asset_emission(
    x_input_amount: float,
    X_virtual_input_side: float,
    Y_virtual_output_side: float,
):
    """
    mint / redeem
    same as `_calc_asset_emission()`
    Source: https://gitlab.com/thorchain/heimdall/-/blob/develop/thorchain/thorchain.py#L1491

    y = (x * X * Y) / (x + X) ** 2

    """
    y_output_amount = (
        x_input_amount * X_virtual_input_side * Y_virtual_output_side
    ) / (x_input_amount + X_virtual_input_side) ** 2

    return y_output_amount


def calc_liq_fee(x_input_amount: float, X_input_side: float, Y_output_side: float):

    """
    f: fee collected
    x: input amount (RUNE or USD)
    X: input side/depth (RUNE or USD)
    Y: output side/depth (RUNE or USD)

    f = (x * x * Y)/(x + X)^2
    """

    fee = (x_input_amount * x_input_amount * Y_output_side) / (
        x_input_amount + X_input_side
    ) ** 2

    return fee


def calc_swap_slip(X_asset_depth: float, x_input_amount: float):
    """
    Calculate the trade slip from a trade
    expressed as a percentage. 0.1 = 10%
    x / (X + x)

    :param int X_asset_depth: first balance, depth of pool
    :param int x_input_amount: asset amount
    :returns: float trade slip

    """
    swap_slip = x_input_amount / (X_asset_depth + x_input_amount)
    return round(swap_slip, 5)  # int(round(swap_slip))


################################################################################
# Policy (Savings)
################################################################################


def save_policy(_params, substep, state_history, previous_state):

    # Check that the previous state vault apy is not less than the min desireable apy
    if previous_state["thor_btc_vault"].APY < _params["min_desireable_apy"]:

        # since no loans only passing previous CR to next block.
        return {"update_vault_apy": previous_state["thor_btc_vault"].APY}

    current_block = previous_state["timestep"]

    # not sure how to iterate users
    # user = previous_state['saver']['user1']
    saver = _params["saver"][current_block]
    # Pretending `lp_asset_amount` is a wallet balance, BTC in this case
    saver_wallet_balance = saver.lp_asset_amount
    btc_pool = previous_state["btc_pool"]
    # btc_vault = previous_state["thor_btc_vault"]
    thor_usd_pool = previous_state["thor_usd_pool"]
    # usd_vault = previous_state["thor_usd_vault"]

    # 1. SWAP asset to RUNE
    rune_amount = calc_asset_emission(
        x_input_amount=saver_wallet_balance,
        X_virtual_input_side=btc_pool.pool_asset_depth,
        Y_virtual_output_side=btc_pool.pool_rune_depth,
    )

    # 2.  Calculate slip from swap, appended to
    # swap slip is appended to list in each virtual pool
    # grab last 300 slip values and use to throttle the pool depth defensively.
    slip_pct = calc_swap_slip(
        X_asset_depth=btc_pool.pool_asset_depth, x_input_amount=saver_wallet_balance
    )  # percent, 0.01 = 1%

    # 3. BURN (swap) rune for thor.btc (mint)
    thor_btc_amount = calc_asset_emission(
        x_input_amount=rune_amount,
        X_virtual_input_side=btc_pool.pool_rune_depth,
        Y_virtual_output_side=btc_pool.pool_asset_depth,
    )

    # 4. Calc thor.btc mint fee
    mint_fee_thor_btc = calc_liq_fee(
        x_input_amount=rune_amount,
        X_input_side=btc_pool.pool_rune_depth,
        Y_output_side=btc_pool.pool_asset_depth,
    )  # output in thor.btc terms.

    # 5. Convert mint fee value to rune
    # get asset anchor price in RUNE
    anchor_price_rune = btc_pool.pool_rune_depth / btc_pool.pool_asset_depth

    mint_fee_rune = anchor_price_rune * mint_fee_thor_btc

    # convert fee rune to thor.usd
    usd_per_rune = thor_usd_pool.pool_asset_depth / thor_usd_pool.pool_rune_depth
    # send mint fees to thor.usd vault in state-update fn
    mint_fee_usd = mint_fee_rune * usd_per_rune

    # 6. Get amount of rune burned to mint the thor.btc
    burned_rune_amt = anchor_price_rune * thor_btc_amount

    # generate a random user id
    fake_user_id = _generate_random_user_id()

    savings_member_entry = {
        "user": fake_user_id,
        "derived_asset_amount": thor_btc_amount,
        "block": current_block,
    }

    policy_update_values = {
        "update_usd_vault_fee_revenue": mint_fee_usd,
        "update_slip_pct": slip_pct,
        "update_user_thor_btc": thor_btc_amount,
        "update_savings_deposit_amt": thor_btc_amount,
        "update_rune_save_deflation": burned_rune_amt,
        "update_savings_member": savings_member_entry,
    }

    return policy_update_values


def thor_usd_save_policy(_params, substep, state_history, previous_state):
    # TODO - add conditional statement where users will add to savings if yield >= x.
    save_amount = _params["save_amount_per_block"]

    policy_update_values = {
        "update_usd_vault_deposit": save_amount,
    }
    return policy_update_values


def thor_usd_withdraw_save_policy(_params, substep, state_history, previous_state):
    # TODO - add conditional statement where users will withdraw with a probability if yield < y.
    thor_usd_vault: ThorUsdVault = previous_state["thor_usd_vault"]

    # we loop through each deposit and withdraw the entire amount
    # for any member whose deposit has crossed the lockup threshold

    # TODO - we assume here that each block only has 1 member, hence creating a dict.
    # Perhaps a seprate id field is cleaner?
    withdrawals: dict = dict(
        [
            (member.block, member.amount)
            for member in thor_usd_vault.members
            if member.amount >= 0.0
            and previous_state["timestep"]
            >= (
                member.block
                + _params.get("savings_lock_up_period", SAVINGS_LOCK_UP_PERIOD)
            )
        ]
    )

    return {
        "update_usd_vault_withdrawal": sum(withdrawals.values()),
        "usd_vault_withdrawals": withdrawals,
    }


def thor_usd_vault_rewards_policy(_params, substep, state_history, previous_state):
    thor_usd_pool: Pool = previous_state["thor_usd_pool"]
    block_rewards_rune = _params["thorchain_block_rewards"]
    # convert block rewards to thor.usd
    usd_per_rune = thor_usd_pool.pool_asset_depth / thor_usd_pool.pool_rune_depth
    rewards_usd = block_rewards_rune * usd_per_rune

    return {
        "update_usd_vault_fee_revenue": rewards_usd,
        "update_rune_supply": block_rewards_rune,
    }


def thor_usd_vault_yield_policy(_params, substep, state_history, previous_state):
    thor_usd_pool: Pool = previous_state["thor_usd_pool"]
    usd_vault_yield_per_block = calc_yield_from_apy(
        thor_usd_pool.pool_yield, BLOCK_TIME_SECONDS, BLOCK_TIME_SECONDS
    )
    usd_per_rune = thor_usd_pool.pool_asset_depth / thor_usd_pool.pool_rune_depth

    # TODO: This should technically be computed on LP units, not the entire pool
    usd_vault_pool_yield_usd = (
        usd_vault_yield_per_block * thor_usd_pool.pool_asset_depth
    )
    usd_vault_pool_yield_rune = (
        usd_vault_yield_per_block * thor_usd_pool.pool_rune_depth
    )

    # TODO:
    # will the rune yield be swapped to thor.usd before getting added?
    # if so when will it be swapped? Might require more complex logic here.

    return {
        # THOR.USD yield + rune yield converted into THOR.USD
        "update_thor_usd_fee_revenue": usd_vault_pool_yield_usd
        + usd_vault_pool_yield_rune * usd_per_rune,
        "update_rune_supply": usd_vault_pool_yield_rune,
    }


def withdraw_save_policy(_params, substep, state_history, previous_state):
    pass


def vault_yield_policy(_params, substep, state_history, previous_state):

    # takes the state of the thor.asset vault, asset pool and calcualtes
    # the yield for each member (pro rata share)

    pass
