from numpy import Infinity

from .constants import SECONDS_IN_YEAR


def calc_yield_from_apy(
    apy: float, duration_seconds: float, block_duration_seconds: float
):
    """
    calculate % yield generated in duration for a given apy and a block rate
    """
    n_y = round(SECONDS_IN_YEAR / block_duration_seconds)
    n = round(duration_seconds / block_duration_seconds)
    r_block = ((apy / 100.0) + 1.0) ** (1.0 / n_y) - 1.0
    return (1 + r_block) ** n - 1


def calc_apy_from_block_yield(block_yield: float, block_duration_seconds: float):
    """
    calculate APY from the reward rate of a single block, given a specific block duration
    """
    n_y = SECONDS_IN_YEAR / block_duration_seconds
    try:
        return (1 + block_yield) ** n_y - 1
    except OverflowError:
        return Infinity
