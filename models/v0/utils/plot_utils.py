import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def _btc_pool_plot(result_df: pd.DataFrame):

    asset_collat = []
    rune_collat = []

    for step in result_df["btc_pool"]:

        asset_collat.append(step.pool_asset_collateral)
        rune_collat.append(step.pool_rune_collateral)

    fig = make_subplots(
        rows=2,
        cols=1,
        # specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        subplot_titles=("Asset Collateral", "Rune Collateral"),
    )

    # Asset collateral
    fig.add_trace(go.Scatter(y=asset_collat, name="BTC"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="BTC", row=1, col=1)
    # rune collateral
    fig.add_trace(go.Scatter(y=rune_collat, name="Rune"), row=2, col=1)
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="Rune", row=2, col=1)

    fig.update_layout(height=500, width=800, title_text="Lending - BTC POOL")

    fig.show()


def pool_collat_utilization_plot(pool_df: pd.DataFrame, pool_name: str):

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    # Total Rune Depth of Pool
    fig.add_trace(
        go.Scatter(y=pool_df["pool_rune_depth"], name="Total Pool Rune Depth"),
        row=1,
        col=1,
        secondary_y=False,
    )
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="Rune", row=1, col=1)

    # Pool Rune Collateral (subset of the total depth)
    fig.add_trace(
        go.Scatter(y=pool_df["pool_rune_collateral"], name="Rune Collat. Depth"),
        row=1,
        col=1,
        secondary_y=False,
    )
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="Rune", row=1, col=1)

    # Loan Collateral as Percentage of Pool depth.
    fig.add_trace(
        go.Scatter(
            y=pool_df["loan_collat_proportion"], name="Loan collat pct. of pool"
        ),
        row=1,
        col=1,
        secondary_y=True,
    )
    fig.update_yaxes(title_text="Collat. Ratio", secondary_y=True)
    fig.update_layout(
        height=500,
        width=800,
        title_text=f"{pool_name} Pool - Depth Collateral Utilization",
    )

    fig.show()


def _thor_usd_vault_plot(result_df: pd.DataFrame):
    fee_revenue = result_df["thor_usd_vault"].map(lambda v: v.fee_revenue)
    apy = result_df["thor_usd_vault"].map(lambda v: v.APY * 100)
    deposits = result_df["thor_usd_vault"].map(lambda v: v.deposits)
    members = dict(
        [(m.block, m.amount) for m in result_df["thor_usd_vault"].iloc[-1].members]
    )
    withdrawals = result_df["thor_usd_vault"].map(lambda v: v.withdrawals)

    fig = make_subplots(
        rows=5,
        cols=1,
        # specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        vertical_spacing=0.15,
        subplot_titles=(
            "Fee Revenue - THOR.USD Savings Vault",
            "Deposits - THOR.USD Savings Vault",
            "Withdrawals - THOR.USD Savings Vault",
            "Fee APY - THOR.USD Savings Vault",
            "Final member balance - THOR.USD Savings Vault",
        ),
    )

    # Fee Revenue
    fig.add_trace(go.Scatter(y=fee_revenue, name="Fee Revenue"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=1, col=1)

    # Deposits
    fig.add_trace(go.Scatter(y=deposits, name="Deposits (THOR.USD)"), row=2, col=1)
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=2, col=1)

    # Withdrawals
    fig.add_trace(
        go.Scatter(y=withdrawals, name="Withdrawals (THOR.USD)"), row=3, col=1
    )
    fig.update_xaxes(title_text="Step (block)", row=3, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=3, col=1)

    # APY
    fig.add_trace(go.Scatter(y=apy, name="APY"), row=4, col=1)
    fig.update_xaxes(title_text="Step (block)", row=4, col=1)
    fig.update_yaxes(title_text="% ($)", row=4, col=1, type="log")

    # Members
    fig.add_trace(
        go.Bar(x=list(members.keys()), y=list(members.values()), name="Members"),
        row=5,
        col=1,
    )
    fig.update_xaxes(title_text="Deposit Block", row=5, col=1)
    fig.update_yaxes(title_text="Balance ($)", row=5, col=1)

    fig.update_layout(height=1300, width=800, title_text="THOR.USD Savings Vault")

    fig.show()


def _lend_module_plot(result_df: pd.DataFrame):

    debt_owed = []
    debt_minted = []

    for step in result_df["lend_module"]:

        debt_owed.append(step.debt_owed)
        debt_minted.append(step.debt_owed)

    fig = make_subplots(
        rows=2,
        cols=1,
        # specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        vertical_spacing=0.25,
        subplot_titles=(
            "Debt Owed (THOR.USD)",
            "Debt Minted (THOR.USD)",
        ),
    )

    # debt_owed
    fig.add_trace(go.Scatter(y=debt_owed, name="Debt Owed (THOR.USD)"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=1, col=1)

    # Debt Minted
    fig.add_trace(
        go.Scatter(y=debt_minted, name="Debt Minted (THOR.USD)"), row=2, col=1
    )
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=2, col=1)

    fig.update_layout(height=500, width=800, title_text="Lend Module Debt Balances")

    fig.show()


def _loan_df_plot(loan_df: pd.DataFrame):

    # debt_owed = []
    # debt_minted = []

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    # debt_owed
    fig.add_trace(
        go.Scatter(y=loan_df["debt_owed"], name="Debt Owed (THOR.USD)"),
        row=1,
        col=1,
        secondary_y=False,
    )
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="THOR.USD ($)", row=1, col=1)

    # Debt Minted
    fig.add_trace(
        go.Scatter(y=loan_df["debt_minted"], name="Loan Value Received (THOR.USD)"),
        row=1,
        col=1,
        secondary_y=False,
    )

    fig.add_trace(
        go.Scatter(y=loan_df["loan_cr"], name="loan CR"), row=1, col=1, secondary_y=True
    )
    fig.update_yaxes(title_text="Collat. Ratio", secondary_y=True)
    fig.update_layout(
        height=500, width=800, title_text="ThorFi - CR <> Loan Relationship"
    )

    fig.show()


def _thor_usd_pool_plot(result_df: pd.DataFrame):

    rune_price = []

    for step in result_df["thor_usd_pool"]:

        rune_price.append(step.asset_price_usd)

    fig = make_subplots(rows=1, cols=1)

    fig.add_trace(go.Scatter(y=rune_price, name="Price, USD"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="USD ($)", row=1, col=1)

    fig.update_layout(height=600, width=800, title_text="Rune Price, USD $")
    fig.show()


def _rune_supply_tracker_plot(result_df: pd.DataFrame):

    rune_supply = []
    rune_inflation = []
    rune_deflation = []

    for step in result_df["rune_supply_tracker"]:

        rune_supply.append(step.rune_supply)
        rune_inflation.append(step.rune_lend_inflation)
        rune_deflation.append(step.rune_save_deflation)

    fig = make_subplots(
        rows=3,
        cols=1,
        # specs=[[{"colspan": 2}], [{"colspan": 2}]],
        horizontal_spacing=0.2,
        vertical_spacing=0.2,
        subplot_titles=(
            "Total Rune Supply",
            "Rune Minted (Lending), cumulative",
            "Rune Burned (Savings), cumulative",
        ),
    )

    # Total Rune Supply
    fig.add_trace(go.Scatter(y=rune_supply, name="Rune Supply"), row=1, col=1)
    fig.update_xaxes(title_text="Step (block)", row=1, col=1)
    fig.update_yaxes(title_text="Rune", row=1, col=1)
    # Rune Inflation
    fig.add_trace(
        go.Scatter(y=rune_inflation, name="Rune Minted (Lending)"), row=2, col=1
    )
    fig.update_xaxes(title_text="Step (block)", row=2, col=1)
    fig.update_yaxes(title_text="Rune", row=2, col=1)

    # Rune Deflation
    fig.add_trace(
        go.Scatter(y=rune_deflation, name="Rune Burned (Savings)"), row=3, col=1
    )
    fig.update_xaxes(title_text="Step (block)", row=3, col=1)
    fig.update_yaxes(title_text="Rune", row=3, col=1)

    fig.update_layout(height=600, width=800, title_text="Rune Supply Tracker")

    fig.show()


def plot_results(
    result_df: pd.DataFrame,
    pool_df: pd.DataFrame,
    pool_name: str,
    loan_df: pd.DataFrame,
):

    _rune_supply_tracker_plot(result_df)
    _thor_usd_pool_plot(result_df)
    _lend_module_plot(result_df)
    _loan_df_plot(loan_df=loan_df)
    # _user_CR_plot(result_df)
    pool_collat_utilization_plot(pool_df=pool_df, pool_name=pool_name)
    _btc_pool_plot(result_df)
    _thor_usd_vault_plot(result_df)
