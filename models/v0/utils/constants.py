SECONDS_IN_YEAR = 365 * 3600 * 24
BLOCK_TIME_SECONDS = 6.0
SAVINGS_LOCK_UP_PERIOD = 50  # number of blocks before which savings cant be taken out
