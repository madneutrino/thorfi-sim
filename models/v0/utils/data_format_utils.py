from dataclasses import asdict
from typing import List

import pandas as pd

# def prep_results(raw_result: List, asset_pool_col: str):
#     def _create_pool_df(
#         result_df: pd.DataFrame, asset_pool_col: str = "btc_pool"
#     ) -> pd.DataFrame:

#         pool_df = pd.DataFrame([asdict(x) for x in result[asset_pool_col].to_list()])
#         pool_df["loan_collat_proportion"] = (
#             pool_df["pool_rune_collateral"] / pool_df["pool_rune_depth"]
#         )

#         return pool_df

#     def _create_loan_df(result_df: pd.DataFrame):
#         all_loans = result["lend_module"].iloc[
#             -1
#         ]  # get last row of lend module, with full loan history.
#         loan_df = pd.DataFrame(all_loans.loans).T.reset_index()
#         loan_df = loan_df.rename(columns={"index": "loan_id"})

#         return loan_df

#     result = pd.DataFrame(raw_result)
#     pool_df = _create_pool_df(result_df=result, asset_pool_col=asset_pool_col)
#     loan_df = _create_loan_df(result_df=result)

#     return result, pool_df, loan_df


def prep_results(raw_result: List, asset_pool_col: str, vault_name_col: str):
    def _create_pool_df(
        result_df: pd.DataFrame, asset_pool_col: str = "btc_pool"
    ) -> pd.DataFrame:

        pool_df = pd.DataFrame([asdict(x) for x in result_df[asset_pool_col].to_list()])
        pool_df["loan_collat_proportion"] = (
            pool_df["pool_rune_collateral"] / pool_df["pool_rune_depth"]
        )

        return pool_df

    def _create_loan_df(result_df: pd.DataFrame):
        all_loans = result_df["lend_module"].iloc[
            -1
        ]  # get last row of lend module, with full loan history.
        loan_df = pd.DataFrame(all_loans.loans).T.reset_index()
        loan_df = loan_df.rename(columns={"index": "loan_id"})

        return loan_df

    def _create_savings_vault_deposit_dfs(
        result_df: pd.DataFrame, vault_col_name: str = "thor_btc_vault"
    ) -> pd.DataFrame:

        vault_df = pd.DataFrame(
            [asdict(x) for x in result_df[vault_col_name].to_list()]
        )

        deposits_df = pd.DataFrame([x for x in vault_df["members"].to_list()])

        return vault_df, deposits_df

    results_dict = {}

    result = pd.DataFrame(raw_result)

    results_dict.update({"result": result})

    if asset_pool_col in result.columns.to_list():

        pool_df = _create_pool_df(result_df=result, asset_pool_col=asset_pool_col)

        results_dict.update({"pool_df": pool_df})

    if "lend_module" in result.columns.to_list():

        loan_df = _create_loan_df(result_df=result)

        results_dict.update({"loan_df": loan_df})

    if vault_name_col in result.columns.to_list():
        vault_df, deposits_df = _create_savings_vault_deposit_dfs(
            result_df=result, vault_col_name=vault_name_col
        )

        results_dict.update({"vault_df": vault_df})
        results_dict.update({"deposits_df": deposits_df})

    return results_dict
